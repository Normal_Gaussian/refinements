# Refinements

[Docs](https://normal_gaussian.gitlab.io/refinements)

Guard unknown data with refinement calls. Typescript compatible.

This is a typescript compatible full pass composable refinement module, based on the premise that a refinement function is of the form.
```ts
<T>(path: Array<string>, v: mixed): T | RefinementError
```

Here the unknown value `v` is refined to the type `T`. If the refinement fails a `RefinementError` is returned. The `path` argument is used to provide a better contextual understanding of where a refinement error occurred.

Refinement functions are commonly called via the provided `attemptRefine` or `useRefine` functions. `attemptRefine` is of the form
```ts
<T>(RefinementFunction<T>, path: Array<string>, v: any, fallback: T) => T
```
while `useRefine` is of the form
```ts
<T>(RefinementFunction<T>) => (path: Array<string>, v: any) => T
```

Standard refinement functions and refinement utility functions are provided.

## canRefine

Useful for type inference without modifying the value 

```js
import { MyType, refineMyType } from './myrefinements';

const x: unknown = {/* ... */}

const canRefineMyType = canRefine(refineMyType);

if(canRefineMyType([], x)) {
    // x is MyType
} else {
    // x is unknown
}
```

# Development

* `yarn run build`

* `yarn run format`

* `yarn run publish`