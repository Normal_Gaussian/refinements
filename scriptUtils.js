const spawn = require("child_process").spawn;
const exec = require("child_process").exec;
const fs = require("fs-extra");
const path = require("path");

if (!process.versions.pnp) {
  console.log("Not using pnp");
} else {
  console.log("Using pnp");
}

async function run(command, arguments) {
  const execMethod = true;
  return new Promise((pass, fail) => {
    const fullCommand = `${command} ${arguments.join(" ")}`;
    const commandString = `"${fullCommand}"`;
    console.log(`Running ${commandString}`);

    let child;
    if (execMethod) {
      console.log("Using exec");
      child = exec(fullCommand, {
        cwd: __dirname,
        stdio: "pipe",
      });
    } else {
      console.log("Using spawn");
      child = spawn(command, arguments, {
        cwd: __dirname,
        stdio: "pipe",
      });
    }

    child.stdout.pipe(process.stdout);
    child.stderr.pipe(process.stderr);

    let output = "";
    child.stdout.on("data", (data) => (output += String(data)));

    let handled = false;
    child.on("exit", exitHandler);
    child.on("error", errorHandler);

    function errorHandler(e) {
      if (handled) {
        return;
      }
      handled = true;

      console.error(`${commandString} encountered an error`);
      fail(e);
    }
    function exitHandler(code) {
      if (handled) {
        return;
      }
      handled = true;

      if (code !== 0) {
        console.error(
          `${commandString} exited with the non-zero exit code "${code}"`
        );
        return fail(
          new Error(
            `${commandString} exited with the non-zero exit code "${code}"`
          )
        );
      }

      console.log(`${commandString} ran successfully`);
      pass(output);
    }
  });
}

async function remove(target) {
  const fullTarget = path.join(__dirname, target);
  if (!fs.existsSync(fullTarget)) {
    return;
  }
  console.log(`Removing ${fullTarget}`);
  await fs.remove(fullTarget);
  console.log(`Removed ${target}`);
}

async function copy(src, target) {
  console.log(`Copying ${src} -> ${target}`);
  await fs.copy(src, target);
  console.log(`Copied ${src} -> ${target}`);
}

function catchAndExit(e) {
  console.error(e);
  console.error("Encountered an error");
  process.exit(1);
}

async function ensureCommitted(e) {
  const data = await run("git", ["status", "--porcelain"]);
  if (data) {
    console.error(`Git repo is not clean`);
    throw new Error(`Git repo is not clean`);
  }
  console.log(`Git repo is clean`);
}

function Version(string) {
  this.parts = string.split(".").map(Number);
  if (this.parts.length !== 3) {
    throw new Error(`"${string}" is not a valid version`);
  }
  if (this.parts.some((v) => Number.isNaN(v))) {
    throw new Error(`"${string}" is not a valid version`);
  }
}
Version.prototype.isNewerThan = function isNewerThan(version) {
  const a = this.parts;
  const b = version.parts;
  // returns true, false, or null
  // true or false if sure, null if unsure
  function newer(x) {
    if (a[x] < b[x]) return false;
    if (b[x] < a[x]) return true;
    return null;
  }
  // Check if it is newer by each position in the parts array
  // if it isn't newer by the end, then it isn't newer at all
  let result;
  for (let i = 0; i < 3; i++) {
    result = newer(i);
    if (result !== null) return result;
  }
  return false;
};
Version.prototype.toString = function toString() {
  return this.parts.join(".");
};

async function isNewVersion() {
  console.log("Cheking version is newer than published versions");
  const packageRaw = await fs.readFile("./package.json");
  const packageJson = JSON.parse(packageRaw);
  const v = new Version(packageJson.version);

  const published = JSON.parse(
    await run("yarn", [
      "npm",
      "info",
      packageJson.name,
      "--fields",
      "versions",
      "--json",
    ])
  ).versions.map((v) => new Version(v));

  const sameOrNewer = published.filter((p) => !v.isNewerThan(p));

  if (sameOrNewer.length) {
    console.error(
      `The current version ${v} is the same or older than the published versions ${sameOrNewer.join(
        ","
      )}`
    );
    throw new Error(
      `The current version ${v} is the same or older than the published versions ${sameOrNewer.join(
        ","
      )}`
    );
  }

  console.log("Version is newer");
  return true;
}

function yarnPublish() {
  return run("yarn", ["npm", "publish", "--access", "public"]);
}

function checkFormatting() {
  return run("yarn", ["run", "check-formatting"]);
}

function compileSource() {
  return remove("lib").then(() =>
    run("yarn", ["run", "tsc", "--project", "tsconfig.build.json"])
  );
}

function testTypes() {
  return run("yarn", [
    "run",
    "tsc",
    "--project",
    "tsconfig.test.json",
    "--noEmit",
  ]);
}

function testJest() {
  return run("yarn", ["run", "jest"]);
}

function yarnInstall() {
  return run("yarn", ["install"]);
}

function generateDocs() {
  return remove("public").then(() =>
    run("yarn", [
      "run",
      "typedoc",
      "--out",
      "public",
      path.join(".", "src", "index.ts"),
    ])
  );
}

module.exports = {
  copy,
  remove,
  testJest,
  testTypes,
  yarnInstall,
  yarnPublish,
  generateDocs,
  catchAndExit,
  isNewVersion,
  compileSource,
  checkFormatting,
  ensureCommitted,
};
