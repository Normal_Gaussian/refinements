"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.refineFunction = exports.isFunction = void 0;
const core_1 = require("./core");
function isFunction(v) {
    return typeof v === "function";
}
exports.isFunction = isFunction;
function refineFunction(path, v) {
    return isFunction(v) ? v : new core_1.RefinementError(path, `to be a function`);
}
exports.refineFunction = refineFunction;
//# sourceMappingURL=function.js.map