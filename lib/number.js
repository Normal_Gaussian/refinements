"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isNumberLiteral = exports.refineNumberLiteral = exports.refineNumber = exports.isNumber = void 0;
const core_1 = require("./core");
function isNumber(v) {
    return typeof v === "number";
}
exports.isNumber = isNumber;
function refineNumber(path, v) {
    return isNumber(v) ? v : new core_1.RefinementError(path, `to be a Number`);
}
exports.refineNumber = refineNumber;
function refineNumberLiteral(literal) {
    return function _refineStringLiteral(path, v) {
        if (!isNumberLiteral(literal, v)) {
            return new core_1.RefinementError(path, `should be number literal "${literal}"`);
        }
        return v;
    };
}
exports.refineNumberLiteral = refineNumberLiteral;
function isNumberLiteral(n, v) {
    return v === n;
}
exports.isNumberLiteral = isNumberLiteral;
//# sourceMappingURL=number.js.map