"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.refineFromEnumArray = exports.refineArrayOf = exports.refineArray = exports.isArray = void 0;
const core_1 = require("./core");
function isArray(v) {
    return Array.isArray(v);
}
exports.isArray = isArray;
function refineArray(path, v) {
    return isArray(v) ? v : new core_1.RefinementError(path, `to be an Array`);
}
exports.refineArray = refineArray;
function refineArrayOf(refineValues) {
    return (path, v) => {
        const array = refineArray(path, v);
        if (core_1.isRefinementError(array)) {
            return array;
        }
        const arrayRefined = new Array(array.length);
        for (let i = 0; i < array.length; i++) {
            const result = refineValues(path.concat(i.toString()), array[i]);
            if (core_1.isRefinementError(result)) {
                return result;
            }
            else {
                arrayRefined[i] = result;
            }
        }
        return arrayRefined;
    };
}
exports.refineArrayOf = refineArrayOf;
/**
 * Given a list of possible values, will ensure that the Array contains only those values
 *
 * An alternate construction for this is `refineFromEnumArray = refineArrayOf(refineEnum)`,
 *  where `refineEnum` is user defined.
 */
function refineFromEnumArray(...enumValues) {
    return (path, v) => {
        const index = enumValues.findIndex((eV) => eV === v);
        return index === -1
            ? new core_1.RefinementError(path, `to be part of the enum "${enumValues.join('", "')}"`)
            : enumValues[index];
    };
}
exports.refineFromEnumArray = refineFromEnumArray;
//# sourceMappingURL=array.js.map