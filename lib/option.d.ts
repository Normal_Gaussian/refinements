import { RefinementError } from "./core";
export declare type Option = null | undefined;
export declare function isOption(v: unknown): v is Option;
/**
 * Returns the defined value of an option type v:?T or the fallback f:T
 */
export declare function option<T>(v: Option | T, f: T): T;
/**
 * refinement function for null or undefined
 *
 * @param path
 * @param v
 */
export declare function refineOption(path: Array<string>, v: unknown): Option | RefinementError;
export declare type Maybe = undefined;
export declare function isMaybe(v: unknown): v is Maybe;
export declare function maybe<T>(v: Maybe | T, f: T): T;
/**
 * Refinement Function for undefined
 *
 * @param path
 * @param v
 */
export declare function refineMaybe(path: Array<string>, v: unknown): Maybe | RefinementError;
/**
 * Refinement Function for undefined
 *
 * @param path
 * @param v
 */
export declare const refineUndefined: typeof refineMaybe;
/**
 * Refinement Function for null
 */
export declare function refineNull(path: Array<string>, v: unknown): null | RefinementError;
//# sourceMappingURL=option.d.ts.map