import { RefinementError, RefinementFunction } from "./core";
export declare function isArray(v: unknown): v is Array<unknown>;
export declare function refineArray(path: Array<string>, v: unknown): Array<unknown> | RefinementError;
export declare function refineArrayOf<T>(refineValues: RefinementFunction<T | RefinementError>): (path: Array<string>, v: unknown) => Array<T> | RefinementError;
/**
 * Given a list of possible values, will ensure that the Array contains only those values
 *
 * An alternate construction for this is `refineFromEnumArray = refineArrayOf(refineEnum)`,
 *  where `refineEnum` is user defined.
 */
export declare function refineFromEnumArray<E>(...enumValues: Array<E>): (path: Array<string>, v: unknown) => RefinementError | E;
//# sourceMappingURL=array.d.ts.map