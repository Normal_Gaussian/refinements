"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isEmptyString = exports.refineStringRegex = exports.isStringLiteral = exports.refineStringLiteral = exports.refineString = exports.isString = void 0;
const core_1 = require("./core");
function isString(v) {
    return typeof v === "string";
}
exports.isString = isString;
function refineString(path, v) {
    return isString(v) ? v : new core_1.RefinementError(path, `to be a String`);
}
exports.refineString = refineString;
function refineStringLiteral(literal) {
    return function _refineStringLiteral(path, v) {
        if (!isStringLiteral(literal, v)) {
            return new core_1.RefinementError(path, `should be string literal "${literal}"`);
        }
        return v;
    };
}
exports.refineStringLiteral = refineStringLiteral;
function isStringLiteral(s, v) {
    return v === s;
}
exports.isStringLiteral = isStringLiteral;
function refineStringRegex(regex) {
    return function _refineStringRegex(path, v) {
        const string = refineString(path, v);
        if (core_1.isRefinementError(string)) {
            return string;
        }
        if (!regex.test(string)) {
            return new core_1.RefinementError(path, `should be a string matching the regex ${regex.toString()}`);
        }
        return string;
    };
}
exports.refineStringRegex = refineStringRegex;
function isEmptyString(v) {
    return typeof v === "string" && v !== "";
}
exports.isEmptyString = isEmptyString;
//# sourceMappingURL=string.js.map