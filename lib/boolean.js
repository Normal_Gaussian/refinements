"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.refineBoolean = exports.isBoolean = void 0;
const core_1 = require("./core");
function isBoolean(v) {
    return typeof v === "boolean";
}
exports.isBoolean = isBoolean;
function refineBoolean(path, v) {
    return isBoolean(v) ? v : new core_1.RefinementError(path, `to be a boolean`);
}
exports.refineBoolean = refineBoolean;
//# sourceMappingURL=boolean.js.map