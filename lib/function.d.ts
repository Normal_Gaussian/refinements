import { RefinementError } from "./core";
export declare function isFunction(v: unknown): v is Function;
export declare function refineFunction(path: Array<string>, v: unknown): Function | RefinementError;
//# sourceMappingURL=function.d.ts.map