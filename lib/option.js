"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.refineNull = exports.refineUndefined = exports.refineMaybe = exports.maybe = exports.isMaybe = exports.refineOption = exports.option = exports.isOption = void 0;
const core_1 = require("./core");
function isOption(v) {
    return v === null || typeof v === "undefined";
}
exports.isOption = isOption;
/**
 * Returns the defined value of an option type v:?T or the fallback f:T
 */
function option(v, f) {
    return isOption(v) ? f : v;
}
exports.option = option;
/**
 * refinement function for null or undefined
 *
 * @param path
 * @param v
 */
function refineOption(path, v) {
    return isOption(v) ? v : new core_1.RefinementError(path, `to be null or undefined`);
}
exports.refineOption = refineOption;
function isMaybe(v) {
    return typeof v === "undefined";
}
exports.isMaybe = isMaybe;
function maybe(v, f) {
    return isMaybe(v) ? f : v;
}
exports.maybe = maybe;
/**
 * Refinement Function for undefined
 *
 * @param path
 * @param v
 */
function refineMaybe(path, v) {
    return isMaybe(v) ? v : new core_1.RefinementError(path, `to be undefined`);
}
exports.refineMaybe = refineMaybe;
/**
 * Refinement Function for undefined
 *
 * @param path
 * @param v
 */
exports.refineUndefined = refineMaybe;
/**
 * Refinement Function for null
 */
function refineNull(path, v) {
    return v === null ? v : new core_1.RefinementError(path, `to be null`);
}
exports.refineNull = refineNull;
//# sourceMappingURL=option.js.map