import { RefinementError, RefinementFunction } from "./core";
/**
 * Determines whether the parameter is an Object (and is not null)
 *
 * @param v value to assess
 * @returns boolean
 */
export declare function isObject(v: unknown): v is {
    [_: string]: unknown;
};
/**
 * Refinement function for Object, regardless of keys and values
 *
 * @param path
 * @param v value to assess
 */
export declare function refineObject(path: Array<string>, v: unknown): {
    [_: string]: unknown;
} | RefinementError;
/**
 * Generates a refinement function for Object with uniformly typed keys and values
 *
 * @param refineKeys RefinementFunction for the keys
 * @param refineValues RefinementFunction for the values
 */
export declare function refineObjectOf<K extends string, V>(refineKeys: RefinementFunction<K | RefinementError>, refineValues: RefinementFunction<V | RefinementError>): (path: Array<string>, v: unknown) => {
    [k in K]: V;
} | RefinementError;
//# sourceMappingURL=object.d.ts.map