/**
 * An object path,
 *
 * Given
 * ```js
 * const a = {
 *     b: {
 *         c: 1
 *     }
 * }
 * ```
 * The path of a is `["a"]`
 *
 * The path of b is `["a", "b"]`
 *
 * The path of c is `["a", "b", "c"]`
 */
export declare type Path = Array<string>;
/**
 * Indicates a refinement having failed;
 *
 * Provides
 * * the object path to the failure [[RefinementError]]#path
 * * the reason for failure [[RefinementError]]#message
 *
 * ```js
 * const path = ["test", "RefinementError", "fields"]
 * const error = new RefinementError(path, "Reason");
 *
 * console.log(error.path === path); // ["test", "RefinementError", "fields"]
 * console.log(error.message); // "Expected test: RefinementError.fields - Reason"
 * ```
 *
 * can be tested for with [[isRefinementError]]
 */
export declare class RefinementError extends Error {
    readonly path: Path;
    /**
     * @param path the path to the part of the refinement where the error occurrs
     * @param message the human readable message describing the nature of the error
     */
    constructor(path: Path, message: string);
}
/**
 * A function that will attempt to refine its input v to its type parameter T.
 *
 * On success it returns the v refined to T.
 *
 * On failure it returns a RefinementError.
 *
 * @param path the current path of v
 * @param v the value to refine
 * @targetParam T the type to refine v to
 */
export declare type RefinementFunction<T> = (path: Path, v: unknown) => T | RefinementError;
export declare type RefinementFunctionType<RF> = RF extends RefinementFunction<infer T> ? T : never;
export declare type RefinementPredicate<T> = (v: unknown) => v is T;
/**
 * A function that will attempt a refinement and on failure use a fallback
 */
export declare type AttemptRefinement<T> = (path: Path, v: unknown) => T;
/**
 * A function that will attempt a refinement and on failure throw an error
 */
export declare type UseRefinement<T> = (path: Path, v: unknown) => T;
/**
 * Create a refinement attempt for a given refinementFunction.
 * A refinement attempt takes a path, value, and fallback, returning the fallback if refining the value fails.
 * attemptRefine does not throw
 *
 * @typeParam T target type
 * @param refine a refinement function for the target type
 * @returns (v: unknown, fallback: T) => T
 */
export declare function attemptRefine<T>(refine: RefinementFunction<T>): (path: Path, v: unknown, fallback: T) => T;
export declare function isRefinementError(v: unknown): v is RefinementError;
/**
 * Generate a RefinementFunction that refines any one of the given refinements
 *
 * @typeParam T the union of all possible refinements
 * @returns RefinementFunction<T>
 */
export declare function refineAny<T>(...refinements: Array<RefinementFunction<T>>): RefinementFunction<T>;
/**
 * Creates a refinement function that throws instead of returning errors; always returns the right type
 *
 * @typeParam T
 * @param refine RefinementFunction<T>
 * @returns (v: unknown) => T
 */
export declare function useRefine<T>(refine: RefinementFunction<T>): (path: Path, v: unknown) => T;
/**
 * Generate a function to test whether a value object can be refined with a given RefinementFunction
 *
 * Said function has a property, getLastError, that will provide its last error
 *
 * @param refine RefinementFunction
 * @returns (path: Path, v: unknown) => boolean
 */
export declare function canRefine<T>(refine: RefinementFunction<T>): {
    getLastError: () => RefinementError;
} & ((path: Path, v: unknown) => v is T);
export declare type RefinementDefinitionBase<B> = B extends RefinementFunction<infer T> ? T : B extends {} ? {
    [K in keyof B]: RefinementDefinitionBase<B[K]>;
} : never;
/**
 * Given a definition made of POJO and refinementFunctions produces a refinement function that refines to that
 *  definition
 */
export declare function makeRefinement<D>(definition: D): RefinementFunction<RefinementDefinitionBase<D>>;
//# sourceMappingURL=core.d.ts.map