"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.refineObjectOf = exports.refineObject = exports.isObject = void 0;
const core_1 = require("./core");
/**
 * Determines whether the parameter is an Object (and is not null)
 *
 * @param v value to assess
 * @returns boolean
 */
function isObject(v) {
    return typeof v === "object" && v !== null;
}
exports.isObject = isObject;
/**
 * Refinement function for Object, regardless of keys and values
 *
 * @param path
 * @param v value to assess
 */
function refineObject(path, v) {
    return isObject(v) ? v : new core_1.RefinementError(path, `to be an Object`);
}
exports.refineObject = refineObject;
/**
 * Generates a refinement function for Object with uniformly typed keys and values
 *
 * @param refineKeys RefinementFunction for the keys
 * @param refineValues RefinementFunction for the values
 */
function refineObjectOf(refineKeys, refineValues) {
    return (path, v) => {
        const obj = refineObject(path, v);
        if (core_1.isRefinementError(obj)) {
            return obj;
        }
        const objKeys = Object.keys(obj);
        const objRefined = {};
        for (let i = 0; i < objKeys.length; i++) {
            const resultKey = refineKeys(path.concat("$Keys"), objKeys[i]);
            if (core_1.isRefinementError(resultKey)) {
                return resultKey;
            }
            const resultValue = refineValues(path.concat(objKeys[i]), obj[objKeys[i]]);
            if (core_1.isRefinementError(resultValue)) {
                return resultValue;
            }
            objRefined[resultKey] = resultValue;
        }
        return objRefined;
    };
}
exports.refineObjectOf = refineObjectOf;
//# sourceMappingURL=object.js.map