import { RefinementError, RefinementFunction } from "./core";
export declare function isString(v: unknown): v is string;
export declare function refineString(path: Array<string>, v: unknown): string | RefinementError;
export declare function refineStringLiteral<S extends string>(literal: S): RefinementFunction<S>;
export declare function isStringLiteral<S extends String>(s: S, v: unknown): v is S;
export declare function refineStringRegex(regex: RegExp): RefinementFunction<string>;
export declare function isEmptyString(v: unknown): v is "";
//# sourceMappingURL=string.d.ts.map