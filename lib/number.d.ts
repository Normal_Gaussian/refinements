import { RefinementError, RefinementFunction } from "./core";
export declare function isNumber(v: unknown): v is number;
export declare function refineNumber(path: Array<string>, v: unknown): number | RefinementError;
export declare function refineNumberLiteral<N extends number>(literal: N): RefinementFunction<N>;
export declare function isNumberLiteral<N extends number>(n: N, v: unknown): v is N;
//# sourceMappingURL=number.d.ts.map