import { RefinementError } from "./core";
export declare function isBoolean(v: unknown): v is boolean;
export declare function refineBoolean(path: Array<string>, v: unknown): boolean | RefinementError;
//# sourceMappingURL=boolean.d.ts.map