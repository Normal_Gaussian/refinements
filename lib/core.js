"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeRefinement = exports.canRefine = exports.useRefine = exports.refineAny = exports.isRefinementError = exports.attemptRefine = exports.RefinementError = void 0;
/**
 * Indicates a refinement having failed;
 *
 * Provides
 * * the object path to the failure [[RefinementError]]#path
 * * the reason for failure [[RefinementError]]#message
 *
 * ```js
 * const path = ["test", "RefinementError", "fields"]
 * const error = new RefinementError(path, "Reason");
 *
 * console.log(error.path === path); // ["test", "RefinementError", "fields"]
 * console.log(error.message); // "Expected test: RefinementError.fields - Reason"
 * ```
 *
 * can be tested for with [[isRefinementError]]
 */
class RefinementError extends Error {
    /**
     * @param path the path to the part of the refinement where the error occurrs
     * @param message the human readable message describing the nature of the error
     */
    constructor(path, message) {
        super(`Expected ${path[0]}: ${path.slice(1).join(".")} - ${message}`);
        this.path = path;
    }
}
exports.RefinementError = RefinementError;
/**
 * Create a refinement attempt for a given refinementFunction.
 * A refinement attempt takes a path, value, and fallback, returning the fallback if refining the value fails.
 * attemptRefine does not throw
 *
 * @typeParam T target type
 * @param refine a refinement function for the target type
 * @returns (v: unknown, fallback: T) => T
 */
function attemptRefine(refine) {
    return function _attemptRefine(path, v, fallback) {
        const result = refine(path, v);
        return isRefinementError(result) ? fallback : result;
    };
}
exports.attemptRefine = attemptRefine;
/* Checks the result of a RefinementFunction
 */
function isRefinementError(v) {
    return v instanceof RefinementError;
}
exports.isRefinementError = isRefinementError;
/**
 * Generate a RefinementFunction that refines any one of the given refinements
 *
 * @typeParam T the union of all possible refinements
 * @returns RefinementFunction<T>
 */
function refineAny(...refinements) {
    return function (path, v) {
        const errors = [];
        for (let i = 0; i < refinements.length; i++) {
            const result = refinements[i](path, v);
            if (isRefinementError(result)) {
                errors.push(result);
            }
            else {
                return result;
            }
        }
        return new RefinementError(path, `\n\t${errors.map((e) => e.message).join("\n OR \n\t")}`);
    };
}
exports.refineAny = refineAny;
/**
 * Creates a refinement function that throws instead of returning errors; always returns the right type
 *
 * @typeParam T
 * @param refine RefinementFunction<T>
 * @returns (v: unknown) => T
 */
function useRefine(refine) {
    return function _useRefine(path, v) {
        const result = refine(path, v);
        if (isRefinementError(result)) {
            throw result;
        }
        else {
            return result;
        }
    };
}
exports.useRefine = useRefine;
/**
 * Generate a function to test whether a value object can be refined with a given RefinementFunction
 *
 * Said function has a property, getLastError, that will provide its last error
 *
 * @param refine RefinementFunction
 * @returns (path: Path, v: unknown) => boolean
 */
function canRefine(refine) {
    let localLastCanRefineRefinementError = new RefinementError([], "Incorrect error fetched");
    function _canRefine(path, v) {
        const result = refine(path, v);
        if (isRefinementError(result)) {
            localLastCanRefineRefinementError = result;
            return false;
        }
        else {
            return true;
        }
    }
    _canRefine.getLastError = function getLastError() {
        return localLastCanRefineRefinementError;
    };
    return _canRefine;
}
exports.canRefine = canRefine;
/**
 * Given a definition made of POJO and refinementFunctions produces a refinement function that refines to that
 *  definition
 */
function makeRefinement(definition) {
    function doRefinement(d, path, v) {
        if (typeof d === "function") {
            // refinement function
            return d(path, v);
        }
        if (typeof v !== "object" || v === null) {
            return new RefinementError(path, `to be an Object`);
        }
        // object of refinement functions
        const o = {};
        for (let [k, vInner] of Object.entries(d)) {
            // @ts-ignore
            const result = doRefinement(vInner, path.concat(k), v[k]);
            if (isRefinementError(result)) {
                return result;
            }
            // @ts-ignore
            o[k] = result;
        }
        // @ts-ignore : unpartialed above...
        return o;
    }
    return (path, v) => doRefinement(definition, path, v);
}
exports.makeRefinement = makeRefinement;
//# sourceMappingURL=core.js.map