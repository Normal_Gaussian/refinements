const {
  ensureCommitted,
  compileSource,
  yarnPublish,
  catchAndExit,
  isNewVersion,
  checkFormatting,
  yarnInstall,
  generateDocs,
  testTypes,
  testJest,
} = require("./scriptUtils");

yarnInstall()
  .then(isNewVersion)
  .catch(catchAndExit)
  .then(checkFormatting)
  .catch(() => {
    console.log(
      "Run `yarn run format` to format all files correctly with Prettier"
    );
    process.exit(1);
  })
  .then()
  .then(testTypes)
  .then(testJest)
  .then(compileSource)
  .then(generateDocs)
  .then(ensureCommitted)
  .then(yarnPublish)
  .then(() => console.log(`Done!`))
  .catch(catchAndExit);
