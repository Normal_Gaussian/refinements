# 0.8.0

* Adds makeRefinement
* Adds testing capability
* Adds tests to array and core

# 0.7.0

* Adds number refinements
* Cleans up string predicates

# 0.6.5

* adds refineFunction and isFunction
* corrects isArray to act as a predicate

# 0.6.4

* adds refineStringRegex

# 0.6.1

* adds path to RefinementError

# 0.6.0

* canRefine now has a per-instance getLastError call
* getLastCanRefineError removed in favour of per-instance call

# Pre 0.6.0

* Creates own repo
