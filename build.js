const {
  yarnInstall,
  compileSource,
  generateDocs,
  catchAndExit,
  testTypes,
  testJest,
} = require("./scriptUtils");

yarnInstall()
  .then(testTypes)
  .then(testJest)
  .then(compileSource)
  .then(generateDocs)
  .then(() => console.log(`Done!`))
  .catch(catchAndExit);
