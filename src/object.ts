import { RefinementError, RefinementFunction, isRefinementError } from "./core";

/**
 * Determines whether the parameter is an Object (and is not null)
 *
 * @param v value to assess
 * @returns boolean
 */
export function isObject(v: unknown): v is { [_: string]: unknown } {
  return typeof v === "object" && v !== null;
}

/**
 * Refinement function for Object, regardless of keys and values
 *
 * @param path
 * @param v value to assess
 */
export function refineObject(
  path: Array<string>,
  v: unknown
): { [_: string]: unknown } | RefinementError {
  return isObject(v) ? v : new RefinementError(path, `to be an Object`);
}

/**
 * Generates a refinement function for Object with uniformly typed keys and values
 *
 * @param refineKeys RefinementFunction for the keys
 * @param refineValues RefinementFunction for the values
 */
export function refineObjectOf<K extends string, V>(
  refineKeys: RefinementFunction<K | RefinementError>,
  refineValues: RefinementFunction<V | RefinementError>
): (path: Array<string>, v: unknown) => { [k in K]: V } | RefinementError {
  return (path: Array<string>, v: unknown) => {
    const obj = refineObject(path, v);
    if (isRefinementError(obj)) {
      return obj;
    }
    const objKeys = Object.keys(obj);
    const objRefined: { [k in K]?: V } = {};
    for (let i = 0; i < objKeys.length; i++) {
      const resultKey = refineKeys(path.concat("$Keys"), objKeys[i]);
      if (isRefinementError(resultKey)) {
        return resultKey;
      }
      const resultValue = refineValues(
        path.concat(objKeys[i]),
        obj[objKeys[i]]
      );
      if (isRefinementError(resultValue)) {
        return resultValue;
      }
      objRefined[resultKey] = resultValue;
    }
    return objRefined as { [k in K]: V };
  };
}
