import { RefinementError } from "./core";

export function isBoolean(v: unknown): v is boolean {
  return typeof v === "boolean";
}
export function refineBoolean(
  path: Array<string>,
  v: unknown
): boolean | RefinementError {
  return isBoolean(v) ? v : new RefinementError(path, `to be a boolean`);
}
