import { RefinementError, RefinementFunction, isRefinementError } from "./core";

export function isString(v: unknown): v is string {
  return typeof v === "string";
}

export function refineString(
  path: Array<string>,
  v: unknown
): string | RefinementError {
  return isString(v) ? v : new RefinementError(path, `to be a String`);
}

export function refineStringLiteral<S extends string>(
  literal: S
): RefinementFunction<S> {
  return function _refineStringLiteral(
    path: Array<string>,
    v: unknown
  ): S | RefinementError {
    if (!isStringLiteral(literal, v)) {
      return new RefinementError(path, `should be string literal "${literal}"`);
    }
    return v;
  };
}

export function isStringLiteral<S extends String>(s: S, v: unknown): v is S {
  return v === s;
}

export function refineStringRegex(regex: RegExp): RefinementFunction<string> {
  return function _refineStringRegex(
    path: Array<string>,
    v: unknown
  ): string | RefinementError {
    const string = refineString(path, v);
    if (isRefinementError(string)) {
      return string;
    }
    if (!regex.test(string)) {
      return new RefinementError(
        path,
        `should be a string matching the regex ${regex.toString()}`
      );
    }
    return string;
  };
}

export function isEmptyString(v: unknown): v is "" {
  return typeof v === "string" && v !== "";
}
