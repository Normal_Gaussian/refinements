import { RefinementError, RefinementFunction, isRefinementError } from "./core";

export function isArray(v: unknown): v is Array<unknown> {
  return Array.isArray(v);
}

export function refineArray(
  path: Array<string>,
  v: unknown
): Array<unknown> | RefinementError {
  return isArray(v) ? v : new RefinementError(path, `to be an Array`);
}

export function refineArrayOf<T>(
  refineValues: RefinementFunction<T | RefinementError>
): (path: Array<string>, v: unknown) => Array<T> | RefinementError {
  return (path: Array<string>, v: unknown) => {
    const array = refineArray(path, v);
    if (isRefinementError(array)) {
      return array;
    }
    const arrayRefined: Array<T> = new Array(array.length);
    for (let i = 0; i < array.length; i++) {
      const result = refineValues(path.concat(i.toString()), array[i]);
      if (isRefinementError(result)) {
        return result;
      } else {
        arrayRefined[i] = result;
      }
    }
    return arrayRefined;
  };
}

/**
 * Given a list of possible values, will ensure that the Array contains only those values
 *
 * An alternate construction for this is `refineFromEnumArray = refineArrayOf(refineEnum)`,
 *  where `refineEnum` is user defined.
 */
export function refineFromEnumArray<E>(
  ...enumValues: Array<E>
): (path: Array<string>, v: unknown) => RefinementError | E {
  return (path: Array<string>, v: unknown) => {
    const index = enumValues.findIndex((eV) => eV === v);
    return index === -1
      ? new RefinementError(
          path,
          `to be part of the enum "${enumValues.join('", "')}"`
        )
      : enumValues[index];
  };
}
