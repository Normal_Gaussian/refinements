import { isArray, refineArray, refineArrayOf } from "./array";
import { refineNumber } from "./number";
import {
  RefinementPredicate,
  RefinementFunction,
  RefinementError,
} from "./core";
import { refineString } from "./string";

describe("array", () => {
  describe("isArray predicate", () => {
    it("should be true for arrays", () => {
      // Arrays
      expect(isArray([])).toBeTruthy();
      expect(isArray([1, 2, 3])).toBeTruthy();
    });
    it("should be false for everything but arrays ", () => {
      expect(isArray(1)).toBeFalsy();
      expect(isArray(0)).toBeFalsy();
      expect(isArray(1.3e9)).toBeFalsy();
      expect(isArray(-1.3e9)).toBeFalsy();
      expect(isArray(NaN)).toBeFalsy();
      expect(isArray(Infinity)).toBeFalsy();
      expect(isArray(undefined)).toBeFalsy();
      expect(isArray(null)).toBeFalsy();
      expect(isArray("")).toBeFalsy();
      expect(isArray("  ")).toBeFalsy();
      expect(isArray("a string that has words in it")).toBeFalsy();
      expect(isArray(true)).toBeFalsy();
      expect(isArray(false)).toBeFalsy();
      expect(isArray({})).toBeFalsy();
      expect(isArray({ "0": 1 })).toBeFalsy();
      expect(isArray({ "0": "1" })).toBeFalsy();
      expect(isArray(BigInt(3))).toBeFalsy();
      expect(isArray(Symbol())).toBeFalsy();
      expect(isArray(new Map())).toBeFalsy();
      expect(isArray(new Set())).toBeFalsy();
      expect(isArray(new Date())).toBeFalsy();
      expect(isArray(Date)).toBeFalsy();
      expect(isArray(Function)).toBeFalsy();
      expect(isArray(Array)).toBeFalsy();
      expect(isArray(Object)).toBeFalsy();
    });
    it("should type as a RefinementPredicate", () => {
      // is a RefinementPredicate
      const p1: RefinementPredicate<unknown[]> = isArray;
      void p1;
    });
  });

  describe("refineArray refinementFunction", () => {
    const path: string[] = [];
    it("should refineArrays", () => {
      expect(refineArray(path, [])).toEqual([]);
      expect(refineArray(path, [1, 2, 3])).toEqual([1, 2, 3]);
    });
    it("should given RefinementError for everything but arrays ", () => {
      expect(refineArray(path, 1)).toBeInstanceOf(RefinementError);
      expect(refineArray(path, 0)).toBeInstanceOf(RefinementError);
      expect(refineArray(path, 1.3e9)).toBeInstanceOf(RefinementError);
      expect(refineArray(path, -1.3e9)).toBeInstanceOf(RefinementError);
      expect(refineArray(path, NaN)).toBeInstanceOf(RefinementError);
      expect(refineArray(path, Infinity)).toBeInstanceOf(RefinementError);
      expect(refineArray(path, undefined)).toBeInstanceOf(RefinementError);
      expect(refineArray(path, null)).toBeInstanceOf(RefinementError);
      expect(refineArray(path, "")).toBeInstanceOf(RefinementError);
      expect(refineArray(path, "  ")).toBeInstanceOf(RefinementError);
      expect(refineArray(path, "a string that has words in it")).toBeInstanceOf(
        RefinementError
      );
      expect(refineArray(path, true)).toBeInstanceOf(RefinementError);
      expect(refineArray(path, false)).toBeInstanceOf(RefinementError);
      expect(refineArray(path, {})).toBeInstanceOf(RefinementError);
      expect(refineArray(path, { "0": 1 })).toBeInstanceOf(RefinementError);
      expect(refineArray(path, { "0": "1" })).toBeInstanceOf(RefinementError);
      expect(refineArray(path, BigInt(3))).toBeInstanceOf(RefinementError);
      expect(refineArray(path, Symbol())).toBeInstanceOf(RefinementError);
      expect(refineArray(path, new Map())).toBeInstanceOf(RefinementError);
      expect(refineArray(path, new Set())).toBeInstanceOf(RefinementError);
      expect(refineArray(path, new Date())).toBeInstanceOf(RefinementError);
      expect(refineArray(path, Date)).toBeInstanceOf(RefinementError);
      expect(refineArray(path, Function)).toBeInstanceOf(RefinementError);
      expect(refineArray(path, Array)).toBeInstanceOf(RefinementError);
      expect(refineArray(path, Object)).toBeInstanceOf(RefinementError);
    });
    it("should type as a RefinementFunction", () => {
      // is a RefinementFunction
      const p1: RefinementFunction<unknown[]> = refineArray;
      void p1;
    });
  });

  describe("refineArrayOf refinementFunction", () => {
    const path: string[] = [];
    it("should refineArrayOf type", () => {
      expect(refineArrayOf(refineNumber)(path, [])).toEqual([]);
      expect(refineArrayOf(refineNumber)(path, [1, 2, 3])).toEqual([1, 2, 3]);
      expect(refineArrayOf(refineString)(path, [])).toEqual([]);
      expect(refineArrayOf(refineString)(path, ["1", "2", "3"])).toEqual([
        "1",
        "2",
        "3",
      ]);
    });
    it("should given RefinementError for everything but arrays of type", () => {
      expect(refineArrayOf(refineNumber)(path, 1)).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, 0)).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, 1.3e9)).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, -1.3e9)).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, NaN)).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, Infinity)).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, undefined)).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, null)).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, "")).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, "  ")).toBeInstanceOf(
        RefinementError
      );
      expect(
        refineArrayOf(refineNumber)(path, "a string that has words in it")
      ).toBeInstanceOf(RefinementError);
      expect(refineArrayOf(refineNumber)(path, true)).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, false)).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, {})).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, { "0": 1 })).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, { "0": "1" })).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, BigInt(3))).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, Symbol())).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, new Map())).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, new Set())).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, new Date())).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, Date)).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, Function)).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, Array)).toBeInstanceOf(
        RefinementError
      );
      expect(refineArrayOf(refineNumber)(path, Object)).toBeInstanceOf(
        RefinementError
      );

      expect(refineArrayOf(refineNumber)(path, ["1", "2", "3"])).toBeInstanceOf(
        RefinementError
      );
    });
    it("result should type as a RefinementFunction", () => {
      // is a RefinementFunction
      const p1: RefinementFunction<unknown[]> = refineArrayOf(refineNumber);
      void p1;
    });
  });
});
