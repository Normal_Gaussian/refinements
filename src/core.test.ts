import { refineArrayOf } from "./array";
import {
  attemptRefine,
  isRefinementError,
  refineAny,
  useRefine,
  canRefine,
  makeRefinement,
  RefinementError,
  Path,
  RefinementFunctionType,
} from "./core";
import { refineNumber } from "./number";
import { refineMaybe } from "./option";
import { refineString } from "./string";

const path: Path = [];

describe("core", () => {
  describe("attemptRefine", () => {
    it("should work in a simple example", () => {
      expect(attemptRefine(refineNumber)(path, 2, 3)).toBe(2);
      expect(attemptRefine(refineNumber)(path, "2", 3)).toBe(3);
    });
  });
  describe("isRefinementError", () => {
    it("should id RefinementErrors", () => {
      expect(isRefinementError(new RefinementError([], ""))).toBe(true);
      expect(isRefinementError(new Error())).toBe(false);
    });
  });
  describe("refineAny", () => {
    it("should work in a simple example", () => {
      expect(refineAny(refineNumber, refineMaybe)(path, undefined)).toBe(
        undefined
      );
      expect(refineAny(refineNumber, refineMaybe)(path, 3)).toBe(3);
      expect(refineAny(refineNumber, refineMaybe)(path, "3")).toBeInstanceOf(
        RefinementError
      );
    });
  });
  describe("useRefine", () => {
    it("should work in a simple example", () => {
      expect(useRefine(refineNumber)(path, 3)).toBe(3);
      expect(() => useRefine(refineNumber)(path, "3")).toThrow(RefinementError);
    });
  });
  describe("canRefine", () => {
    it("should work in a simple example", () => {
      expect(canRefine(refineNumber)(path, 3)).toBe(true);
      expect(canRefine(refineNumber)(path, "3")).toBe(false);

      // types
      const x: unknown = 3;
      if (canRefine(refineNumber)(path, x)) {
        const y: number = x;
        void y;
      } else {
        // @ts-expect-error: x might not be a number
        const z: number = x;
        void z;
      }
    });
  });
  describe("makeRefinement", () => {
    it("should work in a simple example", () => {
      expect(makeRefinement(refineNumber)(path, 3)).toEqual(3);
      expect(makeRefinement(refineNumber)(path, "3")).toBeInstanceOf(
        RefinementError
      );

      const refineExample = makeRefinement({
        number: refineNumber,
        string: refineString,
        objectMap: {
          number: refineNumber,
          string: refineString,
        },
        array: refineArrayOf(
          makeRefinement({
            number: refineNumber,
          })
        ),
      });

      const validValue: RefinementFunctionType<typeof refineExample> = {
        number: 1,
        string: "2",
        objectMap: {
          number: 3,
          string: "4",
        },
        array: [
          {
            number: 5,
          },
        ],
      };
      expect(refineExample(path, validValue)).toEqual(validValue);

      const extraFields: RefinementFunctionType<typeof refineExample> = {
        number: 1,
        string: "2",
        // @ts-expect-error: Extra field date
        date: new Date(),
        objectMap: {
          number: 3,
          string: "4",
        },
        array: [
          {
            number: 5,
          },
        ],
      };
      expect(refineExample(path, extraFields)).toEqual(validValue);
      expect(refineExample(path, extraFields)).not.toHaveProperty("date");

      const missingField: RefinementFunctionType<typeof refineExample> = {
        number: 1,
        string: "2",
        // @ts-expect-error: missing string field
        objectMap: {
          number: 3,
        },
        array: [
          {
            number: 5,
          },
        ],
      };
      expect(refineExample(path, missingField)).toBeInstanceOf(RefinementError);
    });
  });
});
