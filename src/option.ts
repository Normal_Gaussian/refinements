import { RefinementError } from "./core";

export type Option = null | undefined;

export function isOption(v: unknown): v is Option {
  return v === null || typeof v === "undefined";
}
/**
 * Returns the defined value of an option type v:?T or the fallback f:T
 */
export function option<T>(v: Option | T, f: T): T {
  return isOption(v) ? f : v;
}

/**
 * refinement function for null or undefined
 *
 * @param path
 * @param v
 */
export function refineOption(
  path: Array<string>,
  v: unknown
): Option | RefinementError {
  return isOption(v) ? v : new RefinementError(path, `to be null or undefined`);
}

export type Maybe = undefined;

export function isMaybe(v: unknown): v is Maybe {
  return typeof v === "undefined";
}

export function maybe<T>(v: Maybe | T, f: T): T {
  return isMaybe(v) ? f : v;
}

/**
 * Refinement Function for undefined
 *
 * @param path
 * @param v
 */
export function refineMaybe(
  path: Array<string>,
  v: unknown
): Maybe | RefinementError {
  return isMaybe(v) ? v : new RefinementError(path, `to be undefined`);
}
/**
 * Refinement Function for undefined
 *
 * @param path
 * @param v
 */
export const refineUndefined = refineMaybe;

/**
 * Refinement Function for null
 */
export function refineNull(
  path: Array<string>,
  v: unknown
): null | RefinementError {
  return v === null ? v : new RefinementError(path, `to be null`);
}
