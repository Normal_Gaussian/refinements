/**
 * An object path,
 *
 * Given
 * ```js
 * const a = {
 *     b: {
 *         c: 1
 *     }
 * }
 * ```
 * The path of a is `["a"]`
 *
 * The path of b is `["a", "b"]`
 *
 * The path of c is `["a", "b", "c"]`
 */
export type Path = Array<string>;

/**
 * Indicates a refinement having failed;
 *
 * Provides
 * * the object path to the failure [[RefinementError]]#path
 * * the reason for failure [[RefinementError]]#message
 *
 * ```js
 * const path = ["test", "RefinementError", "fields"]
 * const error = new RefinementError(path, "Reason");
 *
 * console.log(error.path === path); // ["test", "RefinementError", "fields"]
 * console.log(error.message); // "Expected test: RefinementError.fields - Reason"
 * ```
 *
 * can be tested for with [[isRefinementError]]
 */
export class RefinementError extends Error {
  readonly path: Path;
  /**
   * @param path the path to the part of the refinement where the error occurrs
   * @param message the human readable message describing the nature of the error
   */
  constructor(path: Path, message: string) {
    super(`Expected ${path[0]}: ${path.slice(1).join(".")} - ${message}`);
    this.path = path;
  }
}

/**
 * A function that will attempt to refine its input v to its type parameter T.
 *
 * On success it returns the v refined to T.
 *
 * On failure it returns a RefinementError.
 *
 * @param path the current path of v
 * @param v the value to refine
 * @targetParam T the type to refine v to
 */
export type RefinementFunction<T> = (
  path: Path,
  v: unknown
) => T | RefinementError;

export type RefinementFunctionType<RF> = RF extends RefinementFunction<infer T>
  ? T
  : never;

export type RefinementPredicate<T> = (v: unknown) => v is T;

/**
 * A function that will attempt a refinement and on failure use a fallback
 */
export type AttemptRefinement<T> = (path: Path, v: unknown) => T;

/**
 * A function that will attempt a refinement and on failure throw an error
 */
export type UseRefinement<T> = (path: Path, v: unknown) => T;

/**
 * Create a refinement attempt for a given refinementFunction.
 * A refinement attempt takes a path, value, and fallback, returning the fallback if refining the value fails.
 * attemptRefine does not throw
 *
 * @typeParam T target type
 * @param refine a refinement function for the target type
 * @returns (v: unknown, fallback: T) => T
 */
export function attemptRefine<T>(
  refine: RefinementFunction<T>
): (path: Path, v: unknown, fallback: T) => T {
  return function _attemptRefine(path: Path, v: unknown, fallback: T): T {
    const result = refine(path, v);
    return isRefinementError(result) ? fallback : result;
  };
}

/* Checks the result of a RefinementFunction
 */
export function isRefinementError(v: unknown): v is RefinementError {
  return v instanceof RefinementError;
}

/**
 * Generate a RefinementFunction that refines any one of the given refinements
 *
 * @typeParam T the union of all possible refinements
 * @returns RefinementFunction<T>
 */
export function refineAny<T>(
  ...refinements: Array<RefinementFunction<T>>
): RefinementFunction<T> {
  return function (path: Path, v: unknown): T | RefinementError {
    const errors: Array<RefinementError> = [];
    for (let i = 0; i < refinements.length; i++) {
      const result = refinements[i](path, v);
      if (isRefinementError(result)) {
        errors.push(result);
      } else {
        return result;
      }
    }
    return new RefinementError(
      path,
      `\n\t${errors.map((e) => e.message).join("\n OR \n\t")}`
    );
  };
}

/**
 * Creates a refinement function that throws instead of returning errors; always returns the right type
 *
 * @typeParam T
 * @param refine RefinementFunction<T>
 * @returns (v: unknown) => T
 */
export function useRefine<T>(
  refine: RefinementFunction<T>
): (path: Path, v: unknown) => T {
  return function _useRefine(path: Path, v: unknown): T {
    const result = refine(path, v);
    if (isRefinementError(result)) {
      throw result;
    } else {
      return result;
    }
  };
}

/**
 * Generate a function to test whether a value object can be refined with a given RefinementFunction
 *
 * Said function has a property, getLastError, that will provide its last error
 *
 * @param refine RefinementFunction
 * @returns (path: Path, v: unknown) => boolean
 */
export function canRefine<T>(
  refine: RefinementFunction<T>
): { getLastError: () => RefinementError } & ((
  path: Path,
  v: unknown
) => v is T) {
  let localLastCanRefineRefinementError: RefinementError = new RefinementError(
    [],
    "Incorrect error fetched"
  );
  function _canRefine(path: Path, v: unknown): v is T {
    const result = refine(path, v);
    if (isRefinementError(result)) {
      localLastCanRefineRefinementError = result;
      return false;
    } else {
      return true;
    }
  }
  _canRefine.getLastError = function getLastError(): RefinementError {
    return localLastCanRefineRefinementError;
  };
  return _canRefine;
}

export type RefinementDefinitionBase<B> = B extends RefinementFunction<infer T>
  ? T
  : B extends {}
  ? { [K in keyof B]: RefinementDefinitionBase<B[K]> }
  : never;

/**
 * Given a definition made of POJO and refinementFunctions produces a refinement function that refines to that
 *  definition
 */
export function makeRefinement<D>(
  definition: D
): RefinementFunction<RefinementDefinitionBase<D>> {
  function doRefinement<D>(
    d: D,
    path: string[],
    v: unknown
  ): RefinementError | RefinementDefinitionBase<D> {
    if (typeof d === "function") {
      // refinement function
      return d(path, v);
    }

    if (typeof v !== "object" || v === null) {
      return new RefinementError(path, `to be an Object`);
    }

    // object of refinement functions
    const o: Partial<RefinementDefinitionBase<D>> = {};
    for (let [k, vInner] of Object.entries(d)) {
      // @ts-ignore
      const result = doRefinement(vInner, path.concat(k), v[k]);
      if (isRefinementError(result)) {
        return result;
      }
      // @ts-ignore
      o[k] = result;
    }
    // @ts-ignore : unpartialed above...
    return o;
  }

  return (path: string[], v: unknown) => doRefinement<D>(definition, path, v);
}
