import { RefinementError, RefinementFunction } from "./core";

export function isNumber(v: unknown): v is number {
  return typeof v === "number";
}

export function refineNumber(
  path: Array<string>,
  v: unknown
): number | RefinementError {
  return isNumber(v) ? v : new RefinementError(path, `to be a Number`);
}

export function refineNumberLiteral<N extends number>(
  literal: N
): RefinementFunction<N> {
  return function _refineStringLiteral(
    path: Array<string>,
    v: unknown
  ): N | RefinementError {
    if (!isNumberLiteral(literal, v)) {
      return new RefinementError(path, `should be number literal "${literal}"`);
    }
    return v;
  };
}

export function isNumberLiteral<N extends number>(n: N, v: unknown): v is N {
  return v === n;
}
