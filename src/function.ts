import { RefinementError } from "./core";

export function isFunction(v: unknown): v is Function {
  return typeof v === "function";
}

export function refineFunction(
  path: Array<string>,
  v: unknown
): Function | RefinementError {
  return isFunction(v) ? v : new RefinementError(path, `to be a function`);
}
